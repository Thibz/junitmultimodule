package squash.tfauto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;

public class ParameterTest {

    @Test
    public void testScopePrem() throws ParameterException {
        int integer = ParameterService.INSTANCE.getTestInt("TC_CUF_testcase");
        Assertions.assertTrue(integer == 50, "The integer value is incorrect, current is" + integer);
        boolean datasetParam = ParameterService.INSTANCE.getTestBoolean("DS_param");
        Assertions.assertTrue(datasetParam, "The dataset param is false");
    }

    @Test
    public void testScopeCom() throws ParameterException {
        int integer = ParameterService.INSTANCE.getTestInt("TC_CUF_testcase");
        Assertions.assertTrue(integer == 50, "The integer value is incorrect, current is" + integer);
        boolean datasetParam = ParameterService.INSTANCE.getTestBoolean("DS_param");
        Assertions.assertTrue(datasetParam, "The dataset param is false");
    }

    @Test
    public void allScope() throws ParameterException {
        int integer = ParameterService.INSTANCE.getInt("TC_CUF_testcase");
        Assertions.assertTrue(integer == 50, "The integer value is incorrect, current is" + integer);
        boolean datasetParam = ParameterService.INSTANCE.getBoolean("DS_param");
        Assertions.assertTrue(datasetParam, "The dataset param is false");
    }

}